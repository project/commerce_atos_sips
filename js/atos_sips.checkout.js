(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.initCashpressoCheckout = {
    attach: function (context) {
      var $atosCheckout = $(context).find('#atos-sips-checkout');

      if ($atosCheckout.length > 0) {
        $atosCheckout.append(drupalSettings.commerce_atos_sips.html);
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
