CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides support for Atos SIPS payment gateway.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_atos_sips

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/commerce_atos_sips


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Drupal Commerce - https://www.drupal.org/project/commerce

Requirements for ATOS SIPS 1.x:

 * ATOS SIPS 1.x binaries
 * Certificates for your Merchant IDs (Test and Live) in PHP format


INSTALLATION
------------

 * Install the Commerce Atos SIPS module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Commerce > Configuration > Payment >
       Payment gateways and Add a new Payment gateway.


MAINTAINERS
-----------

 * Bertrand Presles (bpresles) - https://www.drupal.org/u/bpresles

Supporting organizations:

 * Niji Development - https://www.drupal.org/niji
