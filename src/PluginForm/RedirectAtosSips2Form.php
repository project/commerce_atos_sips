<?php

namespace Drupal\commerce_atos_sips\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Worldline\Sips\Common\SipsEnvironment;
use Worldline\Sips\Paypage\PaypageRequest;
use Worldline\Sips\SipsClient;

/**
 * Form to submit payment to ATOS SIPS.
 *
 * @package Drupal\commerce_atos_sips\PluginForm
 */
class RedirectAtosSips2Form extends PaymentOffsiteForm {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm(
      $form,
      $form_state
    );

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $sipsClient = new SipsClient(
      new SipsEnvironment($configuration['environment']),
      $configuration['merchant_id'],
      $configuration['key_version'],
      $configuration['key_secret']
    );

    $paypageRequest = new PaypageRequest();
    $paypageRequest->setAmount($payment->getAmount()->getNumber());
    $paypageRequest->setCurrencyCode($payment->getAmount()->getCurrencyCode());
    $paypageRequest->setNormalReturnUrl($form['#return_url']);
    $paypageRequest->setAutomaticResponseUrl($payment_gateway_plugin->getNotifyUrl());
    $paypageRequest->setOrderChannel("INTERNET");

    try {
      $initializationResponse = $sipsClient->initialize($paypageRequest);

      if ($initializationResponse->getResponseCode() === 200) {
        $post_data = [
          'redirectionVersion' => $initializationResponse->getRedirectionVersion(
          ),
          'redirectionData' => $initializationResponse->getRedirectionData(),
        ];

        return $this->buildRedirectForm(
          $form,
          $form_state,
          $initializationResponse->getRedirectionUrl(),
          $post_data,
          self::REDIRECT_POST
        );
      }
    }
    catch(\Exception $exception) {
      return FALSE;
    }

    return FALSE;
  }
}
