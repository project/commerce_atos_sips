<?php

namespace Drupal\commerce_atos_sips\PluginForm;

use Drupal\commerce_atos_sips\Plugin\Commerce\PaymentGateway\AtosSips1;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to submit payment to ATOS SIPS.
 *
 * @package Drupal\commerce_atos_sips\PluginForm
 */
class RedirectAtosSips1Form extends PaymentOffsiteForm {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $filesystem;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * RedirectAtosSips1Form constructor.
   */
  public function __construct() {
    $this->renderer = \Drupal::service('renderer');
    $this->filesystem = \Drupal::service('file_system');;
    $this->logger = \Drupal::service('logger.channel.default');;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();

    /** @var AtosSips1 $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $parcomGenerated = $payment_gateway_plugin->generateOrderParcomFile($form, $payment, $order);

    if ($parcomGenerated !== FALSE) {
      $pathfileGenerated = $payment_gateway_plugin->generatePathfileOrder($order);

      if ($pathfileGenerated !== FALSE) {
        try {
          $payment_html = $payment_gateway_plugin->request(
            $order,
            $payment->getAmount()->getNumber()
          );

          $form['atos_sips_checkout'] = [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#attributes' => ['id' => 'atos-sips-checkout'],
          ];

          $form['#attached']['drupalSettings']['commerce_atos_sips'] = [
            'html' => $payment_html,
          ];

          $form['#attached']['library'][] = 'commerce_atos_sips/checkout';

          return $form;
        } catch (\Exception $e) {
          return FALSE;
        }
      }
    }

    return FALSE;
  }
}
