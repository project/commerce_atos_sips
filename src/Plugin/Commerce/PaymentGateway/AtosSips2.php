<?php

namespace Drupal\commerce_atos_sips\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Worldline\Sips\Common\SipsEnvironment;
use Worldline\Sips\SipsClient;

/**
 * Provides an Atos SIPS payment gateway for version 2.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_atos_sips_two",
 *   label = @Translation("Atos SIPS 2.0 (Redirect to payment site)"),
 *   display_label= @Translation("Atos SIPS 2.0"),
 *   forms = {
 *    "offsite-payment" = "Drupal\commerce_atos_sips\PluginForm\RedirectAtosSips2Form"
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *    "mastercard", "visa",
 *   },
 * )
 *
 * @package Drupal\commerce_atos_sips\Plugin\Commerce\PaymentGateway
 */
class AtosSips2 extends OffsitePaymentGatewayBase implements SupportsNotificationsInterface {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @{inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.channel.default')
    );
  }

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    LoggerInterface $logger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'environment' => '',
      'merchant_id' => '',
      'key_version' => 0,
      'key_secret' => ''
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm(
      $form,
      $form_state
    );

    $form['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#description' => $this->t('The Atos SIPS environment for current site instance (SIMU, TEST or PROD).'),
      '#options' => [
        'SIMU' => $this->t('Simulation'),
        'TEST' => $this->t('Test'),
        'PROD' => $this->t('Production'),
      ],
      '#default_value' => $this->configuration['environment'],
      '#required' => TRUE,
    ];

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('The Atos SIPS merchant identifier for your site.'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['key_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key version'),
      '#description' => $this->t('The Atos SIPS key version for your site.'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['key_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key secret'),
      '#description' => $this->t('The Atos SIPS key secret for your site.'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    parent::submitConfigurationForm(
      $form,
      $form_state
    );

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['environment'] = $values['environment'];
    $this->configuration['merchant_id'] = $values['merchant_id'];
    $this->configuration['key_version'] = $values['key_version'];
    $this->configuration['key_secret'] = $values['key_secret'];
  }

  /**
   * {@inheritDoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);
  }

  /**
   * {@inheritDoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);

    $sipsClient = new SipsClient(
      new SipsEnvironment($this->configuration['environment']),
      $this->configuration['merchant_id'],
      $this->configuration['key_secret'],
      $this->configuration['key_version']
    );
    $sipsPaymentResponse = $sipsClient->finalizeTransaction();
    $payment = $this->entityTypeManager->getStorage('commerce_payment')->create([
      'state' => 'authorization',
      'amount' => $sipsPaymentResponse->getAmount(),
      'payment_gateway' => $this->entityId,
      'order_id' => $sipsPaymentResponse->getOrderId(),
      'test' => $this->getMode() === 'test',
      'remote_id' => $sipsPaymentResponse->getAuthorisationId(),
      'remote_state' => ($sipsPaymentResponse->getResponseCode() === '00' ? 'paid' : $sipsPaymentResponse->getResponseCode()),
      'authorized' => $this->time->getRequestTime(),
    ]);

    $payment->save();

    return new Response('', 200);
  }

  /**
   * {@inheritDoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
  }
}
