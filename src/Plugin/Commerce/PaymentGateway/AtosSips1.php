<?php

namespace Drupal\commerce_atos_sips\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides an Atos SIPS payment gateway for version 1.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_atos_sips_one",
 *   label = @Translation("Atos SIPS 1.0 (Redirect to payment site)"),
 *   display_label= @Translation("Atos SIPS 1.0"),
 *   forms = {
 *    "offsite-payment" = "Drupal\commerce_atos_sips\PluginForm\RedirectAtosSips1Form",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *    "mastercard", "visa",
 *   }
 * )
 *
 * @package Drupal\commerce_atos_sips\Plugin\Commerce\PaymentGateway
 */
class AtosSips1 extends OffsitePaymentGatewayBase implements SupportsNotificationsInterface {
  /**
   * Base Atos files path.
   */
  public const BASE_ATOS_FILES_PATH = 'public://commerce_atos_sips';

  private const RESPONSE_CODE_SUCCESS = '00';

  private const RESPONSE_CODES = [
    self::RESPONSE_CODE_SUCCESS => 'Authorization succeeded',
    '02' => 'Authorization requires manual phone validation, because of an overrun of the expenses limit',
    '03' => 'merchant_id field is invalid',
    '05' => 'Authorization refused',
    '12' => 'Invalid transaction',
    '17' => 'Canceled by the user',
    '30' => 'Formatting error',
    '34' => 'Fraud suspicion',
    '75' => 'Number of attempts exceeded',
    '90' => 'Service temporarily unavailable'
  ];

  private const CURRENCY_CODE_MAPPING = [
    '978' => 'EUR',
    '840' => 'USD',
    '826' => 'GBP',
    '032' => 'ARS',
    '040' => 'ATS',
    '036' => 'AUD',
    '056' => 'BEF',
    '986' => 'BRL',
    '124' => 'CAD',
    '756' => 'CHF',
    '280' => 'DEM',
    '208' => 'DKK',
    '246' => 'FIM',
    '300' => 'GRD',
    '392' => 'JPY',
    '116' => 'KHR',
    '410' => 'KRW',
    '484' => 'MXN',
    '578' => 'NOK',
    '554' => 'NZD',
    '752' => 'SEK',
    '702' => 'SGD',
    '792' => 'TRL',
    '901' => 'TWD',
  ];

  private const COUNTRIES_MAPPING = [
    'en' => 'England',
    'fr' => 'France',
    'ge' => 'Germany',
    'it' => 'Italy',
    'sp' => 'Spain',
    'be' => 'Belgium',
    'da' => 'Denmark',
    'fi' => 'Finland',
    'sw' => 'Sweden',
    'po' => 'Portugal',
    'lu' => 'Luxembourg',
    'gr' => 'Greece',
  ];

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $filesystem;

  /**
   * @{inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('file_system'),
      $container->get('renderer'),
      $container->get('messenger'),
      $container->get('logger.channel.default')
    );
  }

  /**
   * AtosSips1 constructor.
   *
   * @param array                                             $configuration
   * @param                                                   $plugin_id
   * @param                                                   $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface    $entity_type_manager
   * @param \Drupal\commerce_payment\PaymentTypeManager       $payment_type_manager
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   * @param \Drupal\Component\Datetime\TimeInterface          $time
   * @param \Drupal\Core\File\FileSystemInterface             $filesystem
   * @param \Drupal\Core\Render\RendererInterface             $renderer
   * @param \Psr\Log\LoggerInterface                          $logger
   * @param \Drupal\Core\Messenger\MessengerInterface         $messenger
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    FileSystemInterface $filesystem,
    RendererInterface $renderer,
    MessengerInterface $messenger,
    LoggerInterface $logger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->logger = $logger;
    $this->filesystem = $filesystem;
    $this->renderer = $renderer;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
        'environment' => '',
        'merchant_id' => '',
        'binary_dir' => '',
        'certif_dir' => '',
        'block_align' => 'center',
        'block_order' => '1,2,3,4,5,6,7,8',
        'currency_code' => '978',
        'header_flag' => '1',
        'lang_code' => 'fr',
        'merchant_country' => 'fr',
        'merchant_lang' => 'fr',
        'accepted_payment_modes' => 'CB,2,VISA,2,MASTERCARD,2',
        'template_name' => '',
        'text_color' => '000000',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm(
      $form,
      $form_state
    );

    $form['environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#description' => $this->t('The Atos SIPS environment for current site instance (Test or Production).'),
      '#options' => [
        'TEST' => $this->t('Test'),
        'PROD' => $this->t('Production'),
      ],
      '#default_value' => $this->configuration['environment'],
      '#required' => TRUE,
    ];

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('The Atos SIPS merchant identifier for your site.'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['binary_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Atos SIPS binaries directory path'),
      '#description' => 'Path to the directory containing the binaries files (binaries should be named "request" and "response")',
      '#default_value' => $this->configuration['binary_dir'],
      '#required' => TRUE,
    ];

    $form['certif_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Certificate directory path'),
      '#description' => $this->t('Path to the directory containing the certificate files (certif filenames should be named: certif.[country_code].[merchant_id].php)'),
      '#default_value' => $this->configuration['certif_dir'],
      '#required' => TRUE,
    ];

    $form['block_align'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Blocks alignment'),
      '#description' => $this->t('Payment page element alignement.'),
      '#default_value' => $this->configuration['block_align'],
      '#required' => TRUE,
    ];

    $form['block_order'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Blocks display order'),
      '#description' => $this->t('The payment page blocks display order.'),
      '#default_value' => $this->configuration['block_order'],
      '#required' => TRUE,
    ];

    $form['currency_code'] = [
      '#type' => 'select',
      '#options' => self::CURRENCY_CODE_MAPPING,
      '#title' => $this->t('Default currency code'),
      '#description' => $this->t('The default currency code.'),
      '#default_value' => $this->configuration['currency_code'],
      '#required' => TRUE,
    ];

    $form['header_flag'] = [
      '#type' => 'checkbox',
      '#options' => [
        '1' => $this->t('Yes'),
        '0' => $this->t('No'),
      ],
      '#title' => $this->t('Display header ?'),
      '#description' => $this->t('Whether the header should be displayed.'),
      '#default_value' => $this->configuration['header_flag'],
      '#required' => TRUE,
    ];

    $form['lang_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default language code'),
      '#description' => $this->t('Default language code for the payment page.'),
      '#default_value' => $this->configuration['lang_code'],
      '#required' => TRUE,
    ];

    $form['merchant_country'] = [
      '#type' => 'select',
      '#title' => $this->t('Merchant country code'),
      '#options' => self::COUNTRIES_MAPPING,
      '#description' => $this->t('The merchant country code in 2 letter format (e.g: fr)'),
      '#default_value' => $this->configuration['merchant_country'],
      '#required' => TRUE,
    ];

    $form['merchant_lang'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant lang'),
      '#description' => $this->t('The merchant lang code in 2 letter format (e.g: fr).'),
      '#default_value' => $this->configuration['merchant_lang'],
      '#required' => TRUE,
    ];

    $form['accepted_payment_modes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List of payment methods accepted'),
      '#description' => $this->t('The list of payment methods accepted as expected by Atos SIPS (e.g: CB,1,VISA,1,MASTERCARD,1).'),
      '#default_value' => $this->configuration['accepted_payment_modes'],
      '#required' => TRUE,
    ];

    $form['template_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The payment page template name'),
      '#description' => $this->t('The template name as expected by Atos SIPS (e.g: template_ca_fr).'),
      '#default_value' => $this->configuration['template_name'],
      '#required' => TRUE,
    ];

    $form['text_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The payment page text color'),
      '#description' => $this->t('The text color on the payment page.'),
      '#default_value' => $this->configuration['text_color'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    parent::submitConfigurationForm(
      $form,
      $form_state
    );

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['environment'] = $values['environment'];
    $this->configuration['merchant_id'] = $values['merchant_id'];
    $this->configuration['binary_dir'] = $values['binary_dir'];
    $this->configuration['certif_dir'] = $values['certif_dir'];
    $this->configuration['block_align'] = $values['block_align'];
    $this->configuration['block_order'] = $values['block_order'];
    $this->configuration['currency_code'] = $values['currency_code'];
    $this->configuration['header_flag'] = $values['header_flag'];
    $this->configuration['lang_code'] = $values['lang_code'];
    $this->configuration['merchant_country'] = $values['merchant_country'];
    $this->configuration['merchant_lang'] = $values['merchant_lang'];
    $this->configuration['accepted_payment_modes'] = $values['accepted_payment_modes'];
    $this->configuration['template_name'] = $values['template_name'];
    $this->configuration['text_color'] = $values['text_color'];

    $this->generateParmcomSeller();
  }

  /**
   * {@inheritDoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);
    $paymentResponse = $this->response($request->request->get('DATA'), $order->order_id->value, 'NORMAL');

    if (!empty($paymentResponse)
      && !empty($paymentResponse['bank_response_code'])
      && $paymentResponse['bank_response_code'] === '00') {
      try {
        $payment = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties([
          'order_id' => $order->order_id->value,
          'remote_id' => $paymentResponse['authorisation_id'],
        ]);

        if (empty($payment)) {
          $this->savePayment($paymentResponse);
        }

        $this->messenger->addMessage('Payment was processed');
        $this->logger->info(
          '[RESPONSE - RETURN] Payment information saved successfully. Transaction reference: @transaction_reference',
          ['@transaction_reference' => $paymentResponse['authorisation_id']]
        );
      }
      catch (\Exception $e) {
        $this->logger->error('[RESPONSE - RETURN] Error : [@error_code] @error_message', [
          '@error_code' => $e->getCode(),
          '@error_message' => $e->getMessage(),
        ]);
      }
    }
    else {
      throw new PaymentGatewayException('Payment was not successful.');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);

    $order_id = $request->query->get('order_id');
    $paymentSucceeded = FALSE;
    try {
      $paymentResponse = $this->response(
        $request->request->get('DATA'),
        $order_id,
        'AUTOMATIC'
      );
      if (!empty($paymentResponse)
        && !empty($paymentResponse['bank_response_code'])
        && $paymentResponse['bank_response_code'] === '00') {
        $payment = $this->entityTypeManager->getStorage('commerce_payment')
          ->loadByProperties(
            [
              'order_id' => $order_id,
              'remote_id' => $paymentResponse['authorisation_id']
            ]
          );

        if (empty($payment)) {
          $this->savePayment($paymentResponse);
        }
      }

      if (!empty($paymentResponse)
        && !empty($paymentResponse['bank_response_code'])) {
        $paymentSucceeded = TRUE;
      }
    }
    catch (\Exception $e) {
      $this->logger->error('[RESPONSE - NOTIFY] Error : [@error_code] @error_message', [
        '@error_code' => $e->getCode(),
        '@error_message' => $e->getMessage(),
      ]);
      $paymentSucceeded = FALSE;
    }

    return new Response('', $paymentSucceeded !== FALSE ? 200 : 500);
  }

  /**
   * {@inheritDoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
  }

  /**
   * Generate a payment request and outputs the HTML to display payment methods
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param float $amount price to pay
   * @return string (HTML - form containing payment methods or message explaining error)
   * @throws \Exception
   */
  public function request(OrderInterface $order, $amount, $activeMultiplePayment = false)
  {
    $plugin_config = $this->getConfiguration();
    $string = 'merchant_id=%s merchant_country=%s currency_code=%s pathfile=%s amount=%s order_id=%s';
    if ($activeMultiplePayment) {
      $string .= ' capture_mode=PAYMENT_N data=NB_PAYMENT=3;PERIOD=30;INITIAL_AMOUNT=' . (round($amount / 3) * 100);
    }
    $params = sprintf(
      $string,
      $plugin_config['merchant_id'],
      $plugin_config['merchant_country'],
      $plugin_config['currency_code'],
      $this->filesystem->realpath($this->getDirectory() . '/pathfile.' . $order->order_id->value),
      str_pad($amount * 100, 3, "0", STR_PAD_LEFT), // $amount MUST be in cents and with 3 char minimum
      $order->order_id->value
    );

    // Check parmcom seller file.
    $this->checkParmcomSellerFile();

    $this->logger->info('[REQUEST - PAYMENT METHODS] Sent parameters : [@params].', ['@params' => $params]);

    //Response format: $response = !code!error!buffer!
    $response = exec(DRUPAL_ROOT . '/' . $this->configuration['binary_dir'] . '/request'. ' ' . escapeshellcmd($params));
    $responseElements = explode('!', $response);
    $htmlPaymentMethods = isset($responseElements[3]) ? $responseElements[3] : null;

    if ($this->handleResponse($responseElements, 'PAYMENT METHODS') && !is_null($htmlPaymentMethods)) {
      return $htmlPaymentMethods;
    }

    return '<p class="text-center">' . $this->t('An error occured during payment.') . '</p>';
  }

  /**
   * Used to handle response from SIPS server and verify return code
   *
   * @param array $response
   * @param string $type
   * @return bool
   */
  public function handleResponse($response, $type)
  {
    /**
     * - code = 0  : response contains HTML in the buffer variable
     * - code = -1 : Error received, check error variable
     */
    $code = isset($response[1]) ? $response[1] : null;
    $error = isset($response[2]) ? $response[2] : null;

    if ($code === null && $error === null) {
      $this->logger->error(
        '[RESPONSE - @type] An error occurred when calling payment service. No data returned.',
        ['@type' => $type]
      );
      return false;
    } else if ($code != 0) {
      $this->logger->error(
        '[RESPONSE - @type] An error occurred when calling payment service : @error',
        ['@type' => $type, '@error' => $error]
      );
      return false;
    }

    //Logging useful data in order to debug later if needed
    $this->logger->info(
      '[RESPONSE - @type] @response_code' . PHP_EOL . '@response_json',
      [
        '@type' => $type,
        '@response_code' => !empty($response[18]) ? self::RESPONSE_CODES[$response[11]] : '',
        '@response_json' => json_encode($response),
      ]
    );

    return true;
  }

  /**
   * Handle payment response
   * @param string $response
   * @param int $order_id
   * @param string $type
   * @throws \Exception
   * @return array
   */
  public function response(string $response, int $order_id, string $type)
  {
    $pathfile_path = $this->filesystem->realpath($this->getDirectory() . '/pathfile.' . $order_id);

    if (empty($response)) {
      $this->logger->error('[RESPONSE] Missing data to process response.');
      throw new \Exception('An error occurred when dealing with payment service.');
    }

    $params = sprintf('message=%s pathfile=%s', $response, $pathfile_path);
    $response = exec(DRUPAL_ROOT . '/' . $this->configuration['binary_dir'] . '/response' . ' ' . escapeshellcmd($params));
    $responseElements = explode('!', $response);

    if ($this->handleResponse($responseElements, $type)) {
      return [
        'merchant_id' => $responseElements[3],
        'merchant_country' => $responseElements[4],
        'amount' => $responseElements[5],
        'transaction_id' => $responseElements[6],
        'payment_means' => $responseElements[7],
        'transmission_date' => $responseElements[8],
        'payment_time' => $responseElements[9],
        'payment_date' => $responseElements[10],
        'response_code' => $responseElements[11],
        'payment_certificate' => $responseElements[12],
        'authorisation_id' => $responseElements[13],
        'currency_code' => $responseElements[14],
        'card_number' => $responseElements[15],
        'cvv_flag' => $responseElements[16],
        'cvv_response_code' => $responseElements[17],
        'bank_response_code' => $responseElements[18],
        'complementary_code' => $responseElements[19],
        'complementary_info' => $responseElements[20],
        'return_context' => $responseElements[21],
        'caddie' => $responseElements[22],
        'receipt_complement' => $responseElements[23],
        'merchant_language' => $responseElements[24],
        'language' => $responseElements[25],
        'customer_id' => $responseElements[26],
        'order_id' => $responseElements[27],
        'customer_email' => $responseElements[28],
        'customer_ip_address' => $responseElements[29],
        'capture_day' => $responseElements[30],
        'capture_mode' => $responseElements[31],
        'data' => $responseElements[32],
        'order_validity' => $responseElements[33],
        'transaction_condition' => $responseElements[34],
        'statement_reference' => $responseElements[35],
        'card_validity' => $responseElements[36],
        'score_value' => $responseElements[37],
        'score_color' => $responseElements[38],
        'score_info' => $responseElements[39],
        'score_threshold' => $responseElements[40],
        'score_profile' => $responseElements[41],
        'threed_ls_code' => $responseElements[43],
        'threed_relegation_code' => $responseElements[44]
      ];
    }

    throw new \Exception('An error occurred when dealing with payment service.');
  }

  /**
   * Check parmcom.[merchant_id] seller file exists or create it.
   */
  private function checkParmcomSellerFile() {
    $path = $this->getDirectory();
    $filename = 'parmcom.' . $this->configuration['merchant_id'];
    // If file does not exist, create it.
    if (!is_file($path . '/' . $filename)) {
      $this->generateParmcomSeller();
    }
  }

  /**
   * Generate Atos pathfile and parmcom.[merchant_id] file according to parameters.
   *
   * @param $form
   *   Config form.
   *
   * @return bool
   */
  private function generateParmcomSeller() {
    $path = $this->getDirectory();

    $parcom_render_array = [
      '#theme' => 'commerce_atos_sips_parmcom_provider',
      '#environment' => $this->configuration['environment'],
      '#merchant_id' => $this->configuration['merchant_id'],
      '#certif_dir' => $this->configuration['certif_dir'],
      '#block_align' => $this->configuration['block_align'],
      '#block_order' => $this->configuration['block_order'],
      '#currency_code' => $this->configuration['currency_code'],
      '#header_flag' => $this->configuration['header_flag'] === '1' ? 'YES' : 'NO',
      '#lang_code' => $this->configuration['lang_code'],
      '#merchant_country' => $this->configuration['merchant_country'],
      '#merchant_lang' => $this->configuration['merchant_lang'],
      '#accepted_payment_modes' => $this->configuration['accepted_payment_modes'],
      '#template_name' => $this->configuration['template_name'],
      '#text_color' => $this->configuration['text_color'],
    ];
    $parcom = (string) $this->renderer->renderPlain($parcom_render_array);
    $parcom = strip_tags($parcom);

    // Save the parmcom file.
    try {
      $parmcom_saved =  $this->filesystem->saveData(
        $parcom,
        $path . '/parmcom.' . $this->configuration['merchant_id'],
        FileSystemInterface::EXISTS_REPLACE
      );
    }
    catch (FileException $fileException) {
      $parmcom_saved = FALSE;
    }

    if ($parmcom_saved !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Generate pathfile for current order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return bool
   */
  public function generatePathfileOrder(OrderInterface $order) {
    $path = $this->getDirectory();

    // Now write datas to the pathfile.
    $pathfile_render_array = [
      '#theme' => 'commerce_atos_sips_pathfile',
      '#debug_mode_enabled' => $this->configuration['environment'] === 'TEST' ? 'YES' : 'NO',
      '#logo_directory_path' => '/' . drupal_get_path('module', 'commerce_atos_sips')
        . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'logos/',
      '#default_parmcom_path' => $this->filesystem->realpath($path . '/parmcom.' . $this->configuration['merchant_id']),
      '#parmcom_path' => $this->filesystem->realpath($path . '/parmcom.' . $order->order_id->value),
      '#certif_path' => DRUPAL_ROOT . '/' . $this->configuration['certif_dir']
        . '/certif',
    ];
    $pathfile = (string) $this->renderer->renderPlain($pathfile_render_array);
    $pathfile = strip_tags($pathfile);

    if (!$this->filesystem->saveData($pathfile, $path . '/pathfile.' . $order->order_id->value, FileSystemInterface::EXISTS_REPLACE)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Generate the shop parcom file for current order.
   *
   * @param $form
   *
   * @return bool|string
   */
  public function generateOrderParcomFile($form, PaymentInterface $payment, OrderInterface $order) {
    $path = $this->getDirectory();
    $notifyUrl = $this->getNotifyUrl();
    $notifyUrl->setOption('query', ['order_id' => $order->order_id->value]);

    // Datas for the parmcom file (seller)
    $parmcom_seller_render_array = [
      '#theme' => 'commerce_atos_sips_parmcom',
      '#auto_response_url' => $notifyUrl->toString(),
      '#cancel_url' => $form['#cancel_url'],
      '#return_url' => $form['#return_url'],

    ];

    $parmcom_seller = (string) $this->renderer->renderPlain($parmcom_seller_render_array);
    $parmcom_seller = strip_tags($parmcom_seller);

    try {
      if (!$this->filesystem->saveData(
        $parmcom_seller,
        $path . '/parmcom.' . $order->order_id->value . '.' . $this->configuration['merchant_id'],
        FileSystemInterface::EXISTS_REPLACE
      )) {
        return FALSE;
      };
    }
    catch (FileException $fileException) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Save a payment for a given paymentResponse.
   *
   * @param $paymentResponse
   *   The payment response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function savePayment($paymentResponse) {
    $statesMapping = [
      self::RESPONSE_CODE_SUCCESS => 'completed',
      '02' => 'authorization',
      'others' => 'voided'
    ];

    $amount = number_format($paymentResponse['amount']/100, 2);
    $price = new Price($amount, self::CURRENCY_CODE_MAPPING[$paymentResponse['currency_code']]);
    $payment = $this->entityTypeManager->getStorage('commerce_payment')
      ->create(
        [
          'state' => !empty($statesMapping[$paymentResponse['bank_response_code']]) ?
            $statesMapping[$paymentResponse['bank_response_code']] : $statesMapping['others'],
          'amount' => $price,
          'payment_gateway' => $this->entityId,
          'order_id' => $paymentResponse['order_id'],
          'test' => $this->getMode() === 'test',
          'remote_id' => $paymentResponse['authorisation_id'],
          'remote_state' => $paymentResponse['bank_response_code']
        ]
      );

    $payment->save();
  }

  /**
   * Get directory to upload and get parcom files and create it if needed.
   *
   * @return bool|string
   */
  private function getDirectory() {
    $path = self::BASE_ATOS_FILES_PATH;
    if ($this->filesystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      return $path;
    }

    return FALSE;
  }
}
